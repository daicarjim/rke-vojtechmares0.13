module "rke-kubernetes" {
  source  = "vojtechmares/rke-kubernetes/hcloud"
  version = "0.1.0"
  # insert the 12 required variables here

  hcloud_token = "VUUkxO97bQwtdvAQ4NDXDCmLLYm4lQvZ87hQezfqNwzufzyXH6GOWcMWcs3xY2Vw"
  ssh_private_key = "~/.ssh/id_rsa"
  ssh_public_key = "~/.ssh/id_rsa.pub"
  location                = "fsn1"
  node_count              = "3"
  node_type               = "cx11"
  node_image              = "ubuntu-20.04"
  load_balancer_type      = "lb11"
  kubernetes_version      = "v1.19.6-rancher1-1"
  kubernetes_cluster_name = "School"
  kubernetes_upgrade_strategy_drain = "false"
  kubernetes_upgrade_strategy_max_unavailable_workers = "one"

}

output "kubeconfig" {
  value     = module.rke-kubernetes.kube_config_yaml
  sensitive = true
}

output "lbipv4" {
  value = module.rke-kubernetes.load_balancer_ipv4
}
